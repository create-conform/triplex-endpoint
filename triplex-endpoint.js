/////////////////////////////////////////////////////////////////////////////////////////////
//
// Triplex Endpoint
//
//    Triplex module that allows you to create an endpoint.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
///////////////////////////////////////////////////////////////////////////////////////////// 
//
// Privates
//
///////////////////////////////////////////////////////////////////////////////////////////// 
var bodyParser =   require("body-parser");
var express =      require("express");
var ntlm;
var ldap;
    Error =        require("error");


function createEndpoint(options) {
    var edp = express();

    ////////////////////////////////////////////////////////////////////
    //
    // Init Body Parser Middleware
    //
    ////////////////////////////////////////////////////////////////////
    // get options
    options.parser = options.parser || {
        limit: "50mb"
    };
    // to support JSON-encoded bodies
    edp.use(bodyParser.json(options.parser));
    // to support URL-encoded bodies
    options.parser.extended = true;
    edp.use(bodyParser.urlencoded(options.parser)); 


    ////////////////////////////////////////////////////////////////////
    //
    // Init NTLM Middleware And Access Control Lists
    //
    ////////////////////////////////////////////////////////////////////
    if (options.authentication && 
        options.authentication.ntlm &&
        options.authentication.ntlm.enable) {
        if ((!options.authentication.ntlm.ldap || !options.authentication.ntlm.ldap.url || !options.authentication.ntlm.ldap.baseDN) && !process.env["logonserver"]) {
            throw "LDAP configuration is missing, please check the config.json.";
        }
        
        options.authentication.ntlm.ldap.url =    options.authentication.ntlm.ldap.url    || "ldap:" + process.env["logonserver"].replace(/\\/gi, "/");
        options.authentication.ntlm.ldap.baseDN = options.authentication.ntlm.ldap.baseDN || "dc=" +   process.env["userdnsdomain"].replace(/\./gi, ",dc=").toLowerCase();
        
        if (!ntlm) ntlm = require("express-ntlm");
        if (!ldap) ldap = require("express-cached-ldap");

        edp.use(ntlm());
    }
    return edp;
}


/////////////////////////////////////////////////////////////////////////////////////////////
//
// TriplexEndpoint Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function TriplexEndpoint(options, shared) {
    if (!(this instanceof TriplexEndpoint)) return new TriplexEndpoint(options, shared);

    var self = this;
    var name;
    var endpoint;
    var server;
    var acl;

    shared =           shared || {};
    shared.endpoints = shared.endpoints || {};
    shared.acl =       shared.acl || {};


    this.options =     options || {};

    ////////////////////////////////////////////////////////////////////
    //
    // Service Controllers
    //
    ////////////////////////////////////////////////////////////////////
    this.start = function() {
        return new Promise(function(resolve, reject) {
            self.options.name = self.options.name || "default";
            self.options.port = self.options.port || 80;

            if (endpoint)                            throw new Error(self.ERROR_ENDPOINT_STARTED, "Endpoint '" + self.options.name + "' is already started.");
            if (shared.endpoints[self.options.name]) throw new Error(self.ERROR_ENDPOINT_EXISTS,  "Endpoint '" + self.options.name + "' is already defined.");

            name =     self.options.name;
            endpoint = createEndpoint(self.options);
            server =   endpoint.listen(self.options.port);

            acl =      self.options.authentication && self.options.authentication.acl? self.options.authentication.acl : [];
            for (var a in acl) {
                if (shared.acl[a]) throw new Error(self.ERROR_ACL_EXISTS, "Acl '" + a + "' is already defined.");
                
                shared.acl[a] = ldap({
                    ldapUrl:      self.options.authentication.ntlm.ldap.url,
                    baseDN:       self.options.authentication.ntlm.ldap.baseDN,
                    ldapUsername: self.options.authentication.ntlm.ldap.username,
                    ldapPassword: self.options.authentication.ntlm.ldap.password,
                    groups:       acl[a],
                    userToBePartOfAllGroups: false
                });
            }
            
            shared.endpoints.add(name, endpoint);

            resolve();
        });
    };

    this.stop = function() {
        return new Promise(function(resolve, reject) {
            if (!endpoint) throw new Error(self.ERROR_ENDPOINT_NOT_STARTED, "Endpoint '" + (name || self.options.name) + "' is not yet started.");
            
            for (var a in acl) {
                if (!shared.acl[a]) throw new Error(self.ERROR_ACL_UNKNOWN, "Acl '" + (name || self.options.name) + "' does not exist.");

                shared.acl[a] = null;
            }

            server.close();
            server =   null;
            endpoint = null;
    
            shared.endpoints.remove(name);
    
            name = null;

            resolve();
        });
    };
};
TriplexEndpoint.prototype.ERROR_ACL_EXISTS =               "Acl Already Exists";
TriplexEndpoint.prototype.ERROR_ACL_UNKNOWN =              "Acl Unknown";
TriplexEndpoint.prototype.ERROR_ENDPOINT_EXISTS =          "Endpoint Already Exists";
TriplexEndpoint.prototype.ERROR_ENDPOINT_STARTED =         "Endpoint Already Started";
TriplexEndpoint.prototype.ERROR_ENDPOINT_NOT_STARTED =     "Endpoint Not Started";


/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = TriplexEndpoint;