# triplex-endpoint

triplex module that enables you to create an endpoint using express. This endpoint can then be used by other modules. Triplex installs this module by default.

## Installation

```sh
npm install triplex-endpoint
```

## Usage

If you want to use this module, you need to add this module to the triplex configuration.

**Hint** You can pass options to triplex in multiple ways. Check the triplex documentation for more information.

```json
{
    "modules" : {
        "triplex-endpoint" : {}
    }
}
```

The module will register an endpoint in triplex shared.endpoints object, which will fire an "add" event when the enpoint is ready for use. For more information, check the triplex documentation.

### Options

| Name | Description | Default |
| ---- | ----------- | ------- |
| name | The name of the endpoint. Other modules will use this name to identify which endpoint to use. | "default" |
| port | The port to listen on for connections. | 80 |
| authentication.ntlm.enable | Enabled NTLM authentication for the endpoint. When supported, clients will automatically send their Windows credentials. | false |
authentication.ntlm.ldap.url | The ldap url for the domain controller. ex. "ldap://DC01". If the value is not specified, and triplex is running on a windows computer and is running under a user account that is part of the domain, the value is automatically set for the user account's domain. | |
| authentication.ntlm.ldap.baseDN | The ldap baseDN string. ex. "dc=acme,dc=com". If the value is not specified, and triplex is running on a windows computer and is running under a user account that is part of the domain, the value is automatically set for the user account's domain. | |

### Example

In the example below, an endpoint with name "intranet" is created that is listening for connections on port 8080 and has ntlm authentication enabled. 

Then, the hsp module is set to use this endpoint.

**Note** that the ldap configuration is omitted, so this will only work on a windows computer while running under a domain user account. If you want to run this example on a linux computer for example, you should check out the configuration options in the opens table above.

```json
{
    "modules" : {
        "triplex-endpoint" : {
            "name" : "intranet",
            "port" : 8080,
            "authentication" : {
                "ntlm" : {
                    "enable" : true
                }
            }
        },
        "triplex-hsp" : {
            "endpoint" : "intranet",
            "path" : "C:\\pub\\www"
        }
    }
}
```
